#include "ToolBase.h"

ToolBase::ToolBase(const std::string &toolName, const std::string &toolId, const std::string &toolCategory, const std::string &toolShortDescription,
                   const std::string &authorFullName, const std::string &authorEmail, CoreInterface *core)
        : core_(core),
          toolName_(toolName),
          toolId_(toolId),
          toolCategory_(toolCategory),
          toolShortDescription_(toolShortDescription),
          authorFullName_(authorFullName),
          authorEmail_(authorEmail){

}