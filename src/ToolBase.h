#ifndef PYTHON_SANDBOX_TOOLBASE_H
#define PYTHON_SANDBOX_TOOLBASE_H


#include "ITool.h"
#include "CoreInterface.h"

class ToolBase : public ITool {
public:
    ToolBase(const std::string& toolName, const std::string& toolId, const std::string& toolCategory, const std::string& toolShortDescription, const std::string& authorFullName, const std::string& authorEmail, CoreInterface* core);
protected:
    CoreInterface *core_;

    std::string toolName_;
    std::string toolId_;
    std::string toolCategory_;
    std::string toolShortDescription_;

    std::string authorFullName_;
    std::string authorEmail_;
};


#endif //PYTHON_SANDBOX_TOOLBASE_H
