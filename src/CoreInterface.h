#ifndef PYTHON_SANDBOX_COREINTERFACE_H
#define PYTHON_SANDBOX_COREINTERFACE_H

#include <string>

class CoreInterface {
public:
    virtual void sendMessage(const std::string& message) = 0;
};


#endif //PYTHON_SANDBOX_COREINTERFACE_H
