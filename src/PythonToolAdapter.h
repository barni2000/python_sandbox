#ifndef PYTHON_SANDBOX_PYTHONTOOLADAPTER_H
#define PYTHON_SANDBOX_PYTHONTOOLADAPTER_H

#include "ToolBase.h"
#include <python3.4/Python.h>
#include <QSettings>

class PythonToolAdapter : public ToolBase{
public:
    PythonToolAdapter(const std::string&packageName, const std::string& toolPath, CoreInterface* core);
    ~PythonToolAdapter();

    void on_load();
    void on_unload();
    void on_reload();
    void on_run();

private:
    void readManifest(const std::string& packageName);
    void importModule();
    void getConstructor();
    void createInstance();

private:
    std::string toolPath_;
    std::string entryClass_;
    PyObject *pyModule_, *pInstance_, *pFunc_;
};


#endif //PYTHON_SANDBOX_PYTHONTOOLADAPTER_H
