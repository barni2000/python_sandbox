#ifndef PYTHON_SANDBOX_ITOOL_H
#define PYTHON_SANDBOX_ITOOL_H

class ITool{
public:
    virtual void on_load() = 0;
    virtual void on_unload() = 0;
    virtual void on_reload() = 0;
    virtual void on_run() = 0;
};

#endif //PYTHON_SANDBOX_ITOOL_H
