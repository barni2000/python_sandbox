#include "ToolFactory.h"
#include "exceptions/NotImplementedException.h"
#include "exceptions/ToolLoadFailedException.h"

#include <QString>

ToolFactory::ToolFactory(CoreInterface* core): core_(core){

}

ITool* ToolFactory::createDummyTool(){
    DummyTool* newDummyTool;
    try {
        newDummyTool = new DummyTool(core_);
        newDummyTool->on_load();
        return newDummyTool;
    } catch (const ToolLoadFailedException& e){
        delete newDummyTool;
    }
}

ITool* ToolFactory::createPythonTool(const std::string& toolName){
    PythonToolAdapter* newPythonTool;
    try{
        newPythonTool = new PythonToolAdapter(toolName, toolboxPath(), core_);
        newPythonTool->on_load();
        return newPythonTool;
    } catch (const ToolLoadFailedException& e){
        delete newPythonTool;
    }
}

std::string ToolFactory::toolboxPath(){
    return std::string("/home/canny/.schtools/tools");
}