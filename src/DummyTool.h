#ifndef PYTHON_SANDBOX_DUMMYTOOL_H
#define PYTHON_SANDBOX_DUMMYTOOL_H

#include "ToolBase.h"


class DummyTool : public ToolBase{
public:
    DummyTool(CoreInterface* core);

    void on_load();
    void on_unload();
    void on_reload();
    void on_run();
};


#endif //PYTHON_SANDBOX_DUMMYTOOL_H
