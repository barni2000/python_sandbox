#ifndef PYTHON_SANDBOX_INIPARSEREXCEPTION_H
#define PYTHON_SANDBOX_INIPARSEREXCEPTION_H

#include <exception>
#include <string>

class IniParserException : public std::exception{
public:
    IniParserException(const std::string& msg): msg_(msg){}
    virtual ~IniParserException() throw(){}

    const char* what() const throw (){
        return msg_.c_str();
    }

protected:
    std::string msg_;
};



#endif //PYTHON_SANDBOX_INIPARSEREXCEPTION_H
