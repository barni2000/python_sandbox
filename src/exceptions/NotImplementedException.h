#ifndef PYTHON_SANDBOX_NOTIMPLEMENTEDEXCEPTION_H
#define PYTHON_SANDBOX_NOTIMPLEMENTEDEXCEPTION_H


#include <stdexcept>

class NotImplementedException : public std::logic_error{
public:
    NotImplementedException(): std::logic_error("Function not implemented yet."){}
};


#endif //PYTHON_SANDBOX_NOTIMPLEMENTEDEXCEPTION_H
