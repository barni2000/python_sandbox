#ifndef PYTHON_SANDBOX_TOOLLOADFAILEDEXCEPTION_H
#define PYTHON_SANDBOX_TOOLLOADFAILEDEXCEPTION_H

#include <bits/stringfwd.h>
#include "ToolLoadException.h"

class ToolLoadFailedException : public ToolLoadException{
public:
    ToolLoadFailedException(const std::string& msg) : ToolLoadException(msg){}
    ToolLoadFailedException() : ToolLoadException(std::string("Failed to load tool.")){}

    virtual ~ToolLoadFailedException() throw(){}
};


#endif //PYTHON_SANDBOX_TOOLLOADFAILEDEXCEPTION_H
