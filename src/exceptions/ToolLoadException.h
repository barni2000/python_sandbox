#ifndef PYTHON_SANDBOX_TOOLLOADEXCEPTION_H
#define PYTHON_SANDBOX_TOOLLOADEXCEPTION_H

#include <exception>

class ToolLoadException : public std::exception{
public:
    ToolLoadException(const std::string& msg): msg_(msg){}
    virtual ~ToolLoadException() throw(){}

    const char* what() const throw (){
        return msg_.c_str();
    }

protected:
    std::string msg_;
};


#endif //PYTHON_SANDBOX_TOOLLOADEXCEPTION_H
