#include <iostream>
#include <python3.4/Python.h>
#include <QDebug>
#include <QTextCodec>

#include "ToolFactory.h"

//static PyObject* test_function(PyObject *self, PyObject *args){
//    std::cout << "test_function called" << std::endl;
//}
//
//static PyMethodDef TestMethods[] = {
//        {
//                "stuff_function",
//                test_function,
//                METH_VARARGS,
//                "Test."
//        },
//        {NULL, NULL, 0, NULL}        /* Sentinel */
//};
//
//static struct PyModuleDef spammodule = {
//        PyModuleDef_HEAD_INIT,
//        "stuff_module",   /* name of module */
//        NULL, /* module documentation, may be NULL */
//        -1,       /* size of per-interpreter state of the module,
//                or -1 if the module keeps state in global variables. */
//        TestMethods
//};
//
//PyMODINIT_FUNC PyInit_spam(void) {
//    return PyModule_Create(&spammodule);
//}

void init() {
//    PyImport_AppendInittab("stuff_module", PyInit_spam);
    Py_SetProgramName((wchar_t *) "python asd");
    Py_Initialize();
    PyRun_SimpleString("import sys");
    PyRun_SimpleString("sys.path.insert(0, \"/home/canny/.schtools/tools\")");
    //PyImport_ImportModule("test_tab");


}

int main(int argc, char *argv[]) {
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(codec);
    init();

    ToolFactory factory(nullptr);
    ITool* dummyTool = factory.createDummyTool();
    ITool* pythonTool = factory.createPythonTool(std::string("schtools_dummy_tool"));

    dummyTool->on_run();
    pythonTool->on_run();

    dummyTool->on_reload();
    pythonTool->on_reload();

    dummyTool->on_unload();
    pythonTool->on_unload();

    delete dummyTool, pythonTool;

    Py_Finalize();

    return 0;
}