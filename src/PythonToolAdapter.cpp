#include "PythonToolAdapter.h"
#include "exceptions/ToolLoadException.h"
#include "exceptions/ToolLoadFailedException.h"
#include "exceptions/IniParserException.h"

#include <QTextCodec>
#include <iostream>
#include <QString>
#include <QDir>
#include <QDebug>
#include <iostream>

PythonToolAdapter::PythonToolAdapter(const std::string &packageName, const std::string &toolPath, CoreInterface *core)
        : ToolBase(std::string(), std::string(), std::string(), std::string(), std::string(), std::string(), core), toolPath_(toolPath) {
    try {
        std::cout << "asd4";
        // Step 0
        readManifest(packageName);

        std::cout << "asd3";
        // Step 1
        importModule();

        std::cout << "asd2";
        // Step 2
        getConstructor();
        std::cout << "asd0";

        // Step 3
        createInstance();
    } catch (const IniParserException & e) {
        std::cerr << e.what();
        throw ToolLoadFailedException();
    } catch (const ToolLoadException &e) {
        std::cerr << e.what();
        throw ToolLoadFailedException();
    }
}

void PythonToolAdapter::readManifest(const std::string& packageName){
    QString filename = QString(toolPath_.c_str()) + QDir::separator() + QString(packageName.c_str()) + QDir::separator() + "manifest.ini";
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    QSettings manifestReader(filename, QSettings::IniFormat);
    manifestReader.setIniCodec(codec);

    toolName_ = manifestReader.value("tool/name").toString().toStdString();
    toolId_ = manifestReader.value("tool/id").toString().toStdString();
    toolCategory_ = manifestReader.value("tool/category").toString().toStdString();
    toolShortDescription_ = manifestReader.value("tool/short_description").toString().toStdString();
    entryClass_ = manifestReader.value("run/entry_class").toString().toStdString();

    PyRun_SimpleString(QString("sys.path.insert(0, \"/home/canny/.schtools/tools/%1\")").arg(QString::fromStdString(toolId_)).toStdString().c_str());
}

void PythonToolAdapter::importModule() {
    pyModule_ = PyImport_ImportModule("main");
    if (pyModule_ == nullptr) {
        PyErr_Print();
        throw ToolLoadException(std::string("Could not import module by the name: ") + toolName_);
    }
}

void PythonToolAdapter::getConstructor() {
    pFunc_ = PyObject_GetAttrString(pyModule_, entryClass_.c_str());
    if (pFunc_ == nullptr && PyCallable_Check(pFunc_)) {
        PyErr_Print();
        throw ToolLoadException("Could not load constructor function.");
    }
}

void PythonToolAdapter::createInstance() {
    std::cout << "asd1";
    pInstance_ = PyObject_CallObject(pFunc_, nullptr);
    if (pInstance_ == nullptr) {
        PyErr_Print();
        throw ToolLoadException("Could not create instance.");
    }
    std::cout << "asd";
}

PythonToolAdapter::~PythonToolAdapter() {
    Py_DECREF(pInstance_);
    Py_DECREF(pyModule_);
    Py_DECREF(pFunc_);
    delete pInstance_, pyModule_, pFunc_;
}

void PythonToolAdapter::on_load() {
    PyObject_CallMethod(pInstance_, "on_load", nullptr);
}

void PythonToolAdapter::on_unload() {
    PyObject_CallMethod(pInstance_, "on_unload", nullptr);
}

void PythonToolAdapter::on_reload() {
    PyObject_CallMethod(pInstance_, "on_reload", nullptr);
}

void PythonToolAdapter::on_run() {
    PyObject_CallMethod(pInstance_, "on_run", nullptr);
}