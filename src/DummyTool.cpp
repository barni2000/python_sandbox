#include "DummyTool.h"

#include <iostream>

DummyTool::DummyTool(CoreInterface *core) : ToolBase(std::string("Tool name"),  std::string("toolId"), std::string("toolCategory"), std::string("toolShortDescription"), std::string("authorFullName"), std::string("authorEmail"),  core){

}

void DummyTool::on_load() {
    std::cout << "DummyTool on load called" << std::endl;
}

void DummyTool::on_unload() {
    std::cout << "DummyTool on unload called" << std::endl;
}

void DummyTool::on_reload() {
    std::cout << "DummyTool on reload called" << std::endl;
}

void DummyTool::on_run() {
    std::cout << "DummyTool on run called" << std::endl;
}