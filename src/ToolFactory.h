#ifndef PYTHON_SANDBOX_TOOLFACTORY_H
#define PYTHON_SANDBOX_TOOLFACTORY_H

#include "DummyTool.h"
#include "PythonToolAdapter.h"
#include "CoreInterface.h"

class ToolFactory {
public:
    ToolFactory(CoreInterface* core);

    ITool* createDummyTool();
    ITool* createPythonTool(const std::string& toolName);

private:
    std::string toolboxPath();
private:
    CoreInterface* core_;
};


#endif //PYTHON_SANDBOX_TOOLFACTORY_H
