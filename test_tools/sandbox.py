class IPlugin:
    # Plugin constructor
    def on_load(self):
        try:
            #from stuff_module import stuff_function
            #stuff_function()
            print('[python] on_load called')
        except Exception as e:
            print(e)

    # Plugin main
    def on_run(self):
        print('[python] on_run called')
        pass

    def on_reload(self):
        print('[python] on_reload called')
        pass

    # Plugin destructor
    def on_unload(self):
        print('[python] on_unload called')
        pass